## Publisher Server
1. [Introduction](#introduction)
2. [Technologies](#technologies)
3. [Setup](#setup)
4. [Installation & Configuration](#installation-and-configuration)
5. [Testing](#testing)
6. [Security Vulnerabilities](#security-vulnerabilities)

### Introduction

This project houses a pub / sub system using HTTP requests

### Technologies

- [PHP](https://www.php.net//)
- [Laravel](https://laravel.com/)
- [Mysql](https://www.mysql.com/)
- [Docker](https://www.docker.com/)


### Setup

**Pre-requisites:**

#### With Docker
* **Docker**: Install [Docker](https://docs.docker.com/get-docker/)
* **For MySQL users**: 5.7.23 or higher.
* **For MariaDB users**: 10.2.7 or Higher.


#### Without Docker
* **OS**: Ubuntu 16.04 LTS or higher / Windows 7 or Higher (WampServer / XAMPP).
* **SERVER**: Apache 2 or NGINX.
* **RAM**: 3 GB or higher.
* **PHP**: 7.2.0 or higher.
* **Processor**: Clock Cycle 1 Ghz or higher.
* **For MySQL users**: 5.7.23 or higher.
* **For MariaDB users**: 10.2.7 or Higher.
* **Node**: 8.11.3 LTS or higher.
* **Composer**: 1.6.5 or higher.

**Configure your database:**

Find file **.env** or create one in your root directory and set the environment variables listed below:

* **APP_URL**
* **DB_CONNECTION**
* **DB_HOST**
* **DB_PORT**
* **DB_DATABASE**
* **DB_USERNAME**
* **DB_PASSWORD**

**Note:**
If you are using Docker and your database is on the same machine, use **docker.host.internal** as the **DB_HOST**

**Change Workspace:**

### Installation
**To start your application**:

#### With Docker

Find file **docker-compose.yml**, **docker-compose-production.yml** and **docker-compose-testing.yml** in your root directory:

##### On server:

The production docker-compose file pulls the latest changes directly from your repository. Find file **docker-compose-production.yml** and set the environment variables listed below:

* **GIT_OAUTH2_TOKEN**
* **REPOSITORY_URL**
* **GIT_BRANCH**

Then run the command below:

```
docker-compose -f docker-compose-production.yml up
```

##### On local:

```
docker-compose up
```

#### Without Docker
- create a .env file from the .env.example file `cp .env.example .env` and fill in the necessary environment variables
- run `composer install` to install dependencies
#### Running Development Server
- Run `php artisan serve` to start the development server
- now access the server on your localhost and the port you provided i.e `localhost:6100 or 127.0.0.1:6100`

### Testing

Find file **.env.testing** in your root directory and set the environment variables listed below:

* **DB_CONNECTION**
* **DB_HOST**
* **DB_PORT**
* **DB_DATABASE**
* **DB_USERNAME**
* **DB_PASSWORD**

**Note:**
If your database is on the same machine, use **docker.host.internal** as the **DB_HOST**

```
composer test
```

This project uses Codeception for testing.  For more information on how to use codeception for testing, visit [Codeception](https://codeception.com/) official website.

### Security Vulnerabilities

If you discover a security vulnerability within this project, please send an e-mail to me [oayomideelijah@gmail.com](mailto:oayomideelijah@gmail.com). All security vulnerabilities will be promptly addressed.
