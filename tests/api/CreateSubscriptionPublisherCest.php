<?php

use Faker\Factory;

class CreateSubscriptionPublisherCest
{
    private $topic;
    private $faker;

    public function _before(ApiTester $I)
    {
        $this->faker = Factory::create();

        $this->topic = $this->faker->word;
    }

    // tests subscribe to topic API
    public function subscribeToTopic(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/subscribe/' . $this->topic, [
          'url' => $this->faker->url
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('"status":"success"');
    }

    // tests cannot subscribe to the same topic with the same url twice
    public function cannotSubscribeToTheSameTopicAndURLTwice(ApiTester $I)
    {
        $topic = $this->faker->word;
        $url = $this->faker->url;
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/subscribe/' . $topic, [
          'url' => $url
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('"status":"success"');

        // second request
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/subscribe/' . $topic, [
          'url' => $url
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('"status":"error"');
    }

    // tests publish message to topic API
    public function publishMessageToTopic(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/publish/' . $this->topic, [
          'message' => $this->faker->text
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('"status":"success"');
    }
}
