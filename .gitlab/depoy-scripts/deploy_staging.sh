#!/usr/bin/env bash
cd "$HOME/publisher-server"
# Remove running container if any
docker-compose -f "docker-compose-${DEPLOY_ENV}.yml" -p "${DEPLOY_ENV}_base" down
# Bring up the container
docker-compose -f "docker-compose-${DEPLOY_ENV}.yml" -p "${DEPLOY_ENV}_base" up -d
