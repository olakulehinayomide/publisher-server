<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Utilities\HttpRequester;
use Exception;

class PublishMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;


    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    protected $url;
    protected $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->url = $data['url'];
        $this->message = $data['message'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(HttpRequester $requester)
    {
        $params = ['message' => $this->message];
        if (! $response = $requester->post($this->url, $params)) {
            // throw exception if request failed so that the job can be retried
            throw new Exception("Error Publishing Event", 1);
        }
    }

    /**
     * Determine the time at which the job should retry.
     *
     * @return number
     */
    public function retryAfter()
    {
        // retry after 30 seconds
        return 30;
    }

    /**
     * Handle a job failure.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        \Log::debug($exception->getMessage());
    }
}
