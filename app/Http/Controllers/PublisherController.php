<?php

namespace App\Http\Controllers;

use App\Subscription;
use App\Http\Requests\Publish;
use App\Jobs\PublishMessage;

class PublisherController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Publish  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Publish $request, $topic)
    {
        $message = $request->validated()['message'];

        $subscriptions = Subscription::topicSubscriptions($topic);

        $this->publishMessageToSubscriptions($message, $subscriptions);

        return response([ 'status' => 'success', 'message' => 'Message published.'], 200);
    }

    private function publishMessageToSubscriptions($message, $subscriptions)
    {
        foreach ($subscriptions as $subscription) {
            $url = $subscription->url;
            $data = [
                'message' => $message,
                'url' => $url
            ];
            PublishMessage::dispatch($data);
        }
    }
}
