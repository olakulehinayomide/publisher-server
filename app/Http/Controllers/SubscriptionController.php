<?php

namespace App\Http\Controllers;

use App\Subscription;
use App\Http\Requests\StoreSubscription;

class SubscriptionController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSubscription  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubscription $request, $topic)
    {
        $data = $request->validated();

        $data = array_merge($data, ['topic' => $topic]);

        $subscription = Subscription::firstOrNew($data);

        if (!$subscription->exists) {
            $subscription->save();
            return response([ 'status' => 'success', 'message' => 'Subscription successful.'], 200);
        }

        return response([ 'status' => 'error', 'message' => 'Subscription already exist.'], 200);
    }
}
