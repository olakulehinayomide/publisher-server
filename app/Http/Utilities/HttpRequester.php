<?php

namespace App\Http\Utilities;

class HttpRequester
{
    /**
     * http post request
     *
     * @param string $url
     * @param array $params
     * @param array $headers
     * @return array
     */
    public function post($url, $params, $headers = null)
    {
        if (!$headers) {
            $headers = [
                'content-type'=>'application/json; charset=utf-8'
            ];
        }
        try {
            $client = new \GuzzleHttp\Client([
                'headers' => $headers,
                'verify' => false,
                'exceptions'=>true
            ]);
            $response = $client->post($url, [
                'json' => $params
            ]);
            $status = $response->getStatusCode();
            $body = $response->getBody();
            $response = json_decode($body);

            if ($status === 200)
                return true;
        } catch (\Exception $e) {
            \Log::debug($e->getMessage());
        }
        return false;
    }
}
