<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{

    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            $model->removeTopicSubscriptionFromCache();
        });

        static::deleted(function ($model) {
            $model->removeTopicSubscriptionFromCache();
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'topic',
        'url'
    ];

    /**
     * Topic subscriptions.
     *
     * @param string      $topic
     *
     * @return array
     */
    public static function topicSubscriptions($topic)
    {
        // GET THE TOPIC SUBSCRIPTIONS - sort collection in blade
        $subscriptions = \Cache::remember('topic_subscriptions_'.$topic, \Carbon\Carbon::now()->addDays(30), function () use ($topic) {
            return static::select('url')
            ->where('topic', $topic)
            ->orderBy('created_at', 'desc')
            ->get();
        });

        return $subscriptions;
    }

    public function removeTopicSubscriptionFromCache()
    {
        \Cache::forget('topic_subscriptions_'.$this->topic);
    }
}
